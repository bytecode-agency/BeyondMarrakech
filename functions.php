<?php

/*
|------------------------------------------------------
|  Framework: Avada child setup
|------------------------------------------------------
*/

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

/*
|------------------------------------------------------
|  WordPress: Change the Login Logo
|------------------------------------------------------
*/
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(/wp-content/themes/beyond-marrakech/logo.png);
            width: 320px;
            background-size: 160px;
			      height: 100px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/*
|------------------------------------------------------
|  WooCommerce: Redirect shop page to custom page
|------------------------------------------------------
*/
add_filter( 'woocommerce_return_to_shop_redirect', "custom_woocommerce_return_to_shop_redirect" ,20 );
function custom_woocommerce_return_to_shop_redirect(){
   return site_url()."catalog";
}

if( $excerpt == 'yes' ) {
 $content .= tf_content( $excerpt_words, $strip_html );
 $content .= '<a href="'.get_permalink(get_the_ID()).'">Read more</a>';
 }

 /*
 |------------------------------------------------------
 |  WooCommerce: Disable reviews
 |------------------------------------------------------
 */
add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}
